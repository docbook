OBJECT = all
SOURCE =  docbook.xml
PARAM = param.xsl
ARG = --output html/
COMPILER = xsltproc

$(OBJECT):$(SOURCE) $(PARAM)
	$(COMPILER) $(ARG) $(PARAM) $(SOURCE)

clean:
	rm -rf ../html/*.html